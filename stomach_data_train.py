#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import Lasso

from sklearn.externals import joblib
from sklearn.metrics import classification_report
#显示所有列
#pd.set_option('display.max_columns', None)
#显示所有行
#pd.set_option('display.max_rows', None)
#设置value的显示长度为100，默认为50
pd.set_option('max_colwidth',100)

# data direct
pharmacy_data='data/whole_data_final_add_time_interval_and_diff_join_ncz_sample.csv'
pharmacy_neg_data='data/whole_data_final_add_time_interval_and_diff_join_ncz_neg_sample_2w.csv'

pharmacy_df=pd.read_csv(pharmacy_data)
pharmacy_neg_df=pd.read_csv(pharmacy_neg_data)

#model direct
logistic_model='models/pharmacy_logistict_regression.m'

#存储或加载模型
def save_model(model,model_path):
    joblib.dump(model, model_path)    
    return 

def load_model(model_path):
    return joblib.load(model_path)

#返回每行缺失值的总数
#pharmacy_df.isnull().sum(axis=1)
#返回每列缺失值的总数
#pharmacy_df.isnull().sum(axis=0)

#sample后的数据
whole_df=pd.concat([pharmacy_df,pharmacy_neg_df],axis=0,ignore_index=True)

#特征转换，将字符串类型特征转换为自然数字
career=whole_df['career']
mechanism=whole_df['mechanism']

career_feature={career_cate:list(career.drop_duplicates().dropna()).index(career_cate) for career_cate in list(career.drop_duplicates().dropna())}
mechanism_feature={mechanism_cate:list(mechanism.drop_duplicates().dropna()).index(mechanism_cate) for mechanism_cate in list(mechanism.drop_duplicates().dropna())}
whole_df['career']=whole_df['career'].map(career_feature)
whole_df['mechanism']=whole_df['mechanism'].map(mechanism_feature)

#特征shuffle
whole_df.sample(frac=1)

#缺失值填充
for column in list(whole_df.columns[whole_df.isnull().sum() > 0]):
    unk_value=999
    whole_df[column].fillna(unk_value, inplace=True)


y=whole_df['if_ncz']
whole_df.drop(['if_ncz'],axis=1,inplace=True)
X=whole_df

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=0)


sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

#feature selection
# We use the base estimator LassoCV since the L1 norm promotes sparsity of features.
select_model_rf = RandomForestClassifier()
select_model_rf.fit(X_train, y_train)
print(select_model_rf.feature_importances_)

#extra tree
select_model_etree = ExtraTreesClassifier()
select_model_etree.fit(X_train, y_train)
print(select_model_etree.feature_importances_)

#lasso
def pretty_print_linear(coefs, names = None, sort = False):
    if names == None:
        names = ["X%s" % x for x in range(len(coefs))]
    lst = zip(coefs, names)
    if sort:
        lst = sorted(lst,  key = lambda x:-np.abs(x[0]))
    return " + ".join("%s * %s" % (round(coef, 3), name)
                                   for coef, name in lst)

lasso = Lasso(alpha=.3)
lasso.fit(X_train, y_train)
print("Lasso model: ", pretty_print_linear(lasso.coef_, None, sort = True))

# train
LR = LogisticRegressionCV(Cs=5,cv=5,random_state=0)
LR.fit(X_train_std,y_train)
save_model(LR, logistic_model)



#test
y_predict=LR.predict(X_test_std)
print(accuracy_score(y_true=y_test, y_pred=y_predict))
wrong_index=[index for index in range(len(y_predict)) if list(y_test)[index]!=y_predict[index]]
wrong_predict_pred_label=[y_predict[index] for index in wrong_index]
wrong_predict_true_label=[list(y_test)[index] for index in wrong_index]

print(classification_report(y_test, y_predict))
print()


