#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
from sklearn.metrics import classification_report
import itertools
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

whole_feature='D:\\python_workspace\\huaxi_program\\predict_result\\logistic_regression_results_using_whole_features.csv'
minus_correlated='D:\\python_workspace\\huaxi_program\\predict_result\\logistic_regression_results_drop_minus_correlated_features.csv'

whole_feature_df=pd.read_csv(whole_feature)
minus_correlated_df=pd.read_csv(minus_correlated)

labels=['康复(A)','可疑(B)','带瘤部分缓解(C)','带瘤稳定(D)','带瘤进展状态(E)','转移、复发、残瘤(F)','因癌症死亡(G)']

print(classification_report(list(whole_feature_df['生存判断']), list(whole_feature_df['prediction'])))
print(classification_report(list(minus_correlated_df['生存判断']), list(minus_correlated_df['prediction'])))



def plot_confusion_matrix(cm, classes, title='Confusion matrix'):
    plt.imshow(cm, interpolation='nearest', cmap=None)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=0)
    plt.yticks(tick_marks, classes)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def plot_matrix(y_true, y_pred):
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_true, y_pred)
    class_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
    plot_confusion_matrix(confusion_matrix
                          , classes=class_names
                          , title='Confusion matrix')    

plot_matrix(list(whole_feature_df['生存判断']), list(whole_feature_df['prediction']))
print()
