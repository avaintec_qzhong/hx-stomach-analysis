#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import Lasso

from sklearn.externals import joblib
from sklearn.metrics import classification_report
#显示所有列
#pd.set_option('display.max_columns', None)
#显示所有行
#pd.set_option('display.max_rows', None)
#设置value的显示长度为100，默认为50
#pd.set_option('max_colwidth',100)

# data direct
stomach_data='data/stomach_data_cleaned_with_label_1125.xlsx'
stomach_data_20191125='data/stomach_data_cleaned_with_label_1125_14times_followup_reginal.xlsx'
stomach_data_20191125_dealed='data/stomach_data_cleaned_with_label_1125_14times_followup_dealed.csv'

stomach_data_20191125_train='data/stomach_data_cleaned_with_label_1125_14times_followup_train.csv'
stomach_data_20191125_test='data/stomach_data_cleaned_with_label_1125_14times_followup_test.csv'

stomach_data_20191125_train_sc='data/stomach_data_cleaned_with_label_1125_14times_followup_train_sc.csv'
stomach_data_20191125_test_sc='data/stomach_data_cleaned_with_label_1125_14times_followup_test_sc.csv'

stomach_df_regi=pd.read_excel(stomach_data)
#stomach_df=pd.read_excel(stomach_data_20191125,sheetname=1)
stomach_df=pd.read_excel(stomach_data_20191125)

stomach_data_20191125_regi_csv='data/stomach_data_cleaned_with_label_1125.csv'
stomach_data_20191125_14times_regi='data/stomach_data_cleaned_with_label_1125_14times_followup.csv'

#将原始数据转为csv存储
#stomach_df_regi.to_csv(stomach_data_20191125_regi_csv)
#stomach_df.to_csv(stomach_data_20191125_14times_regi)

res = {}
num=0
for i in stomach_df.isnull().sum(axis=1):
    if i>=0 and i<=10:
        res[num]='0-10'
        num+=1
    if i>10 and i<=20:
        res[num]='10-20'
        num+=1
    if i>20 and i<=30:
        res[num]='20-30'
        num+=1
    if i>30 and i<=40:
        res[num]='30-40'
        num+=1
    if i>40:
        res[num]='>40'
        num+=1
        
for key in res.keys():
    #print(key)
    if res[key]=='>40':
        #print(stomach_df.iloc[key].isnull().sum())
        stomach_df=stomach_df.drop([key])

stomach_df_cea=stomach_df[['初期CEA','辅后CEA','术后CEA','CEA','CEA.1','CEA.2', 'CEA.3', 'CEA.4', 'CEA.5', 'CEA.6', 'CEA.7', 
                           'CEA.8', 'CEA.9','CEA.10','CEA.11','CEA.12','CEA.13']]
stomach_df_followup=stomach_df[['随访1', '随访2','随访3','随访4','随访5','随访6','随访7','随访8','随访9','随访10','随访11','随访12','随访13','随访14']]
stomach_df_servive=stomach_df[['生存','生存.1','生存.2','生存.3','生存.4','生存.5','生存.6','生存.7','生存.8','生存.9','生存.10','生存.11','生存状态','生存状态.1']]
stomach_df_label=stomach_df['生存判断']




#特征工程
#CEA值进行按行均值进行缺失值填充
colmean = stomach_df_cea.mean(axis=1)
for i, col in enumerate(stomach_df_cea):
    stomach_df_cea.iloc[:, i] = stomach_df_cea.iloc[:, i].fillna(colmean)
stomach_df_cea=stomach_df_cea.fillna(0)

#将首次随访时间切分为年、月、日
stomach_df_followup_1st_fu=pd.concat([pd.to_datetime(stomach_df_followup['随访1']).dt.year,pd.to_datetime(stomach_df_followup['随访1']).dt.month,pd.to_datetime(stomach_df_followup['随访1']).dt.day],axis=1)
#缺失值填充
stomach_df_followup_1st_fu=stomach_df_followup_1st_fu.fillna(999)

#对第二次以后的随访时间进行变换,计算其与第一次随访时间的间隔
stomach_df_followup_chg=pd.concat([(pd.to_datetime(stomach_df_followup['随访2']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访3']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访4']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访5']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访6']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访7']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访8']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访9']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访10']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访11']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访12']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访13']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days,
(pd.to_datetime(stomach_df_followup['随访14']) - pd.to_datetime(stomach_df_followup['随访1'])).dt.days],axis=1)
#缺失值填充
stomach_df_followup_chg=stomach_df_followup_chg.fillna(0)




#特征转换，将字符串类型特征转换为自然数字
#所有生存类别聚合在一起去重
survive_state = []
for cols in stomach_df_servive.columns:
    survive_state+=list(stomach_df_servive[cols].unique())

survive_feature={'A1':0,'a1':0,'A2':1,'A3':2,'A4':3,'A5':4,'b1':5,'B1':5,'B2':6,'b2':6,'B3':7,'C1':7,'C2':8,'C3':9,'C4':10,'C5':11,
                 'C6':12,'C7':13,'C8':14,'C9':15,'D1':16,'D2':17,'D3':18,'D4':19,'E1':20,'E2':21,'F1':22,'癌性死亡':22,'F2':23}
survive_label={'A1':0,'a1':0,'A2':0,'A3':0,'A4':0,'A5':0,'b1':1,'B1':1,'B2':1,'b2':1,'B3':1,'C1':1,'C2':2,'C3':5,
               'C5':3,'C6':4,'C7':4,'C8':4,'C9':6,'D1':0,'D2':0,'D3':0,'D4':0,'E1':2,'E2':3,'F1':6,'癌性死亡':6,'F2':0}


for i, col in enumerate(stomach_df_servive):
    stomach_df_servive[col]=stomach_df_servive[col].map(survive_feature)
stomach_df_label=stomach_df_label.map(survive_label)

#缺失值填充
#生存标记
stomach_df_servive=stomach_df_servive.fillna(999)

#df_final=pd.concat([stomach_df_cea,stomach_df_followup_1st_fu,stomach_df_followup_chg,stomach_df_servive,stomach_df_label],axis=1)

df_final=pd.concat([stomach_df_cea['初期CEA'],stomach_df_cea['辅后CEA'],stomach_df_cea['术后CEA'],stomach_df_followup_1st_fu,stomach_df_cea['CEA'],stomach_df_servive['生存']
,stomach_df_followup_chg[0],stomach_df_cea['CEA.1'],stomach_df_servive['生存.1']
,stomach_df_followup_chg[1],stomach_df_cea['CEA.2'],stomach_df_servive['生存.2']
,stomach_df_followup_chg[2],stomach_df_cea['CEA.3'],stomach_df_servive['生存.3']
,stomach_df_followup_chg[3],stomach_df_cea['CEA.4'],stomach_df_servive['生存.4']
,stomach_df_followup_chg[4],stomach_df_cea['CEA.5'],stomach_df_servive['生存.5']
,stomach_df_followup_chg[5],stomach_df_cea['CEA.6'],stomach_df_servive['生存.6']
,stomach_df_followup_chg[6],stomach_df_cea['CEA.7'],stomach_df_servive['生存.7']
,stomach_df_followup_chg[7],stomach_df_cea['CEA.8'],stomach_df_servive['生存.8']
,stomach_df_followup_chg[8],stomach_df_cea['CEA.9'],stomach_df_servive['生存.9']
,stomach_df_followup_chg[9],stomach_df_cea['CEA.10'],stomach_df_servive['生存.10']
,stomach_df_followup_chg[10],stomach_df_cea['CEA.11'],stomach_df_servive['生存.11']
,stomach_df_followup_chg[11],stomach_df_cea['CEA.12'],stomach_df_servive['生存状态']
,stomach_df_followup_chg[12],stomach_df_cea['CEA.13'],stomach_df_servive['生存状态.1'],stomach_df_label],axis=1)

y=stomach_df_label
X=pd.concat([stomach_df_cea['初期CEA'],stomach_df_cea['辅后CEA'],stomach_df_cea['术后CEA'],stomach_df_followup_1st_fu,stomach_df_cea['CEA'],stomach_df_servive['生存']
,stomach_df_followup_chg[0],stomach_df_cea['CEA.1'],stomach_df_servive['生存.1']
,stomach_df_followup_chg[1],stomach_df_cea['CEA.2'],stomach_df_servive['生存.2']
,stomach_df_followup_chg[2],stomach_df_cea['CEA.3'],stomach_df_servive['生存.3']
,stomach_df_followup_chg[3],stomach_df_cea['CEA.4'],stomach_df_servive['生存.4']
,stomach_df_followup_chg[4],stomach_df_cea['CEA.5'],stomach_df_servive['生存.5']
,stomach_df_followup_chg[5],stomach_df_cea['CEA.6'],stomach_df_servive['生存.6']
,stomach_df_followup_chg[6],stomach_df_cea['CEA.7'],stomach_df_servive['生存.7']
,stomach_df_followup_chg[7],stomach_df_cea['CEA.8'],stomach_df_servive['生存.8']
,stomach_df_followup_chg[8],stomach_df_cea['CEA.9'],stomach_df_servive['生存.9']
,stomach_df_followup_chg[9],stomach_df_cea['CEA.10'],stomach_df_servive['生存.10']
,stomach_df_followup_chg[10],stomach_df_cea['CEA.11'],stomach_df_servive['生存.11']
,stomach_df_followup_chg[11],stomach_df_cea['CEA.12'],stomach_df_servive['生存状态']
,stomach_df_followup_chg[12],stomach_df_cea['CEA.13'],stomach_df_servive['生存状态.1']],axis=1)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=0)

sc = StandardScaler()
sc.fit(X_train)
X_train_std = pd.DataFrame(sc.transform(X_train))
X_test_std = pd.DataFrame(sc.transform(X_test))


stomach_df_train=pd.concat([X_train,y_train],axis=1)
stomach_df_test=pd.concat([X_test,y_test],axis=1)

#stomach_df_train.to_csv(stomach_data_20191125_train)
#stomach_df_test.to_csv(stomach_data_20191125_test)


#X_train_std.to_csv(stomach_data_20191125_train_sc)
#X_test_std.to_csv(stomach_data_20191125_test_sc)

df_final.to_csv(stomach_data_20191125_dealed)


#model direct
logistic_model='models/stomach_logistict_regression.m'

#存储或加载模型
def save_model(model,model_path):
    joblib.dump(model, model_path)    
    return 

def load_model(model_path):
    return joblib.load(model_path)

#返回每行缺失值的总数
#stomach_df.isnull().sum(axis=1)
#返回每列缺失值的总数
#stomach_df.isnull().sum(axis=0)

#sample后的数据
whole_df=pd.concat([pharmacy_df,pharmacy_neg_df],axis=0,ignore_index=True)

#特征转换，将字符串类型特征转换为自然数字
career=whole_df['career']
mechanism=whole_df['mechanism']

career_feature={career_cate:list(career.drop_duplicates().dropna()).index(career_cate) for career_cate in list(career.drop_duplicates().dropna())}
mechanism_feature={mechanism_cate:list(mechanism.drop_duplicates().dropna()).index(mechanism_cate) for mechanism_cate in list(mechanism.drop_duplicates().dropna())}
whole_df['career']=whole_df['career'].map(career_feature)
whole_df['mechanism']=whole_df['mechanism'].map(mechanism_feature)

#特征shuffle
whole_df.sample(frac=1)

#缺失值填充
for column in list(whole_df.columns[whole_df.isnull().sum() > 0]):
    unk_value=999
    whole_df[column].fillna(unk_value, inplace=True)


y=whole_df['if_ncz']
whole_df.drop(['if_ncz'],axis=1,inplace=True)
X=whole_df

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=0)


sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

#feature selection
# We use the base estimator LassoCV since the L1 norm promotes sparsity of features.
select_model_rf = RandomForestClassifier()
select_model_rf.fit(X_train, y_train)
print(select_model_rf.feature_importances_)

#extra tree
select_model_etree = ExtraTreesClassifier()
select_model_etree.fit(X_train, y_train)
print(select_model_etree.feature_importances_)

#lasso
def pretty_print_linear(coefs, names = None, sort = False):
    if names == None:
        names = ["X%s" % x for x in range(len(coefs))]
    lst = zip(coefs, names)
    if sort:
        lst = sorted(lst,  key = lambda x:-np.abs(x[0]))
    return " + ".join("%s * %s" % (round(coef, 3), name)
                                   for coef, name in lst)

lasso = Lasso(alpha=.3)
lasso.fit(X_train, y_train)
print("Lasso model: ", pretty_print_linear(lasso.coef_, None, sort = True))

# train
LR = LogisticRegressionCV(Cs=5,cv=5,random_state=0)
LR.fit(X_train_std,y_train)
save_model(LR, logistic_model)



#test
y_predict=LR.predict(X_test_std)
print(accuracy_score(y_true=y_test, y_pred=y_predict))
wrong_index=[index for index in range(len(y_predict)) if list(y_test)[index]!=y_predict[index]]
wrong_predict_pred_label=[y_predict[index] for index in wrong_index]
wrong_predict_true_label=[list(y_test)[index] for index in wrong_index]

print(classification_report(y_test, y_predict))
print()


