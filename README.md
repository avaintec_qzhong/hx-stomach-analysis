# README #

### What is this repository for? ###

华西胃肠外科实验程序

### what are the folds for ###

data: 存放原始、中间数据的目录

model：存放模型的目录

predict_result：存放预测结果的目录

### The function of python footages ###

stomach_data_analysis.py: 最初提供的数据的数据分析、统计

stomach_data_analysis_14times_followup.py: 取了14次随访的数据分析、统计

stomach_data_evaluate.py: 模型预测程序

stomach_data_train.py: 模型训练程序


### How do I get set up? ###
模型训练：python stomach_data_train.py
模型预测：python stomach_data_evaluate.py




